# docker-coq-gitlab-ci-demo-1

[![pipeline status](https://gitlab.com/erikmd/docker-coq-gitlab-ci-demo-1/badges/master/pipeline.svg)](https://gitlab.com/erikmd/docker-coq-gitlab-ci-demo-1/commits/master)

Demo of [Docker-Coq](https://hub.docker.com/r/coqorg/coq/) + [GitLab CI](./.gitlab-ci.yml).
